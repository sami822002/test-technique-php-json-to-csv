# Test technique PHP Json to  Csv


# Description
========================

 Former 2 deux CSV distincts, à partir du fichier sample_data_test.json localisé à la racine du projet :

    teams.csv
    team_members.csv

# Requirements
==============

  * PHP 7.4
  * Symfony 5.4
  * composer

# Installation
==============

git clone https://gitlab.com/sami822002/test-technique-php-json-to-csv.git test-technique

# Usage
=======

``` cd test-technique ```

``` composer install ```

### Sous Windows 
- Demarer le serveur Symfony  avec la commande

``` php -S 127.0.0.1:8000 -t public ```

puis lancer le projet à l'url :

 ``` http://127.0.0.1:8000/teams ```

### Sous Linux:

Configurer vhost (DocumentRoot,Directory) pour pointer sur le dossier public du projet
```
  DocumentRoot /var/www/public
 <Directory /var/www/public>
```

# Tests
=======

Vous aurez deux bouttons:

- [ ]  Recuperer teams.csv
- [ ]  Recuperer team_members.csv 

Description de l'application
----------------------------

**le projet contient:**

## Class TeamService:  

elle permet de : 

- [ ] Recuperer le contenu json du fichier sample_data_test
- [ ] Transformer les données Json en tableau associatif.
- [ ] Construire un tableau associatif team personnalisé
- [ ] Construire à partir du tableau team deux chaines format CSV  pour team et team_members respectivement.
- [ ] utiliser le fichier ```mapping.csv```  dans le traitement du fichier team_members.csv

## Class TeamsController:

elle contient : 

- [ ] action index : elle renvoie une vue twig avec deux bouttons
- [ ] action getCSV : elle recupere les Données CSV et renvoie une reponse (donlowad du fichier )

## Class RequestEvent:

- [ ] Definir un alias pour le service TeamService dans ```service.yaml ```
```
  teamRequestListener:
    alias: App\Service\TeamsService
    public: true
```
- [ ] Elle ecoute un evenement(``onKernelRequest ``  de ``RequestEvent``)
- [ ] Si la route est ``app_teams``, lancer le traitement de la class TeamService

